<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<!--
This XSLT is intended for use with the book production platform Editoria, by
the Coko Foundation, as a means to export book content into InDesign for page
placement, where a high degree of manual control over the layout is required.

It was modified from an existing XSL file "tkbr2icml-v045.xsl" from the Ickmull
project (https://code.google.com/archive/p/ickmull/), created by John W. Maxwell,
Meghan MacDonald, and Travis Nicholson at Simon Fraser University's Master of
Publishing program. Many thanks for their efforts.
-->

<xsl:stylesheet xmlns:xhtml="http://www.w3.org/1999/xhtml"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                exclude-result-prefixes="xhtml"
                version="1.0">
  <xsl:output indent="yes"/>

  <xsl:param name="table-width">540</xsl:param>

  <!-- Fixed strings used to indicate ICML and software version -->
  <xsl:variable name="icml-decl-pi">
    <xsl:text>style="50" type="snippet" readerVersion="6.0" featureSet="257" product="6.0(352)"</xsl:text> <!-- product string will change with specific InDesign builds (but probably doesn't matter) -->
  </xsl:variable>
  <xsl:variable name="snippet-type-pi">
    <xsl:text>SnippetType="InCopyInterchange"</xsl:text>
  </xsl:variable>


  <!-- Default Rule: Match everything, ignore it,  and keep going "down". -->
  <xsl:template match="@*|node()">
    <xsl:apply-templates select="@*|node()"/>
  </xsl:template>

  <xsl:variable name="principal-root" select="/"/>

  <!-- ==================================================================== -->
  <!-- Document root generation and boilerplate. -->
  <!-- ==================================================================== -->
  <xsl:template match="xhtml:body">
    <xsl:processing-instruction name="aid"><xsl:value-of select="$icml-decl-pi"/></xsl:processing-instruction>
    <xsl:processing-instruction name="aid"><xsl:value-of select="$snippet-type-pi"/></xsl:processing-instruction>
    <Document DOMVersion="6.0" Self="xhtml2icml_document">
      <RootCharacterStyleGroup Self="xhtml2icml_character_styles">
        <CharacterStyle Self="CharacterStyle/normal" Name="normal"/>
        <CharacterStyle Self="CharacterStyle/link" Name="link"/>
        <CharacterStyle Self="CharacterStyle/i" Name="italic"/>
        <CharacterStyle Self="CharacterStyle/b" Name="bold"/>
        <CharacterStyle Self="CharacterStyle/superscript" Name="superscript"/>
        <CharacterStyle Self="CharacterStyle/subscript" Name="subscript"/>
        <CharacterStyle Self="CharacterStyle/small-caps" Name="small-caps"/>
        <!-- Generate the rest of the CharacterStyles using the @class value -->
        <xsl:apply-templates select="//xhtml:span[@class]" mode='character-style'/>
      </RootCharacterStyleGroup>
      <RootParagraphStyleGroup Self="xhtml2icml_paragraph_styles">
        <ParagraphStyle Self="ParagraphStyle/h1" Name="h1"/>
        <ParagraphStyle Self="ParagraphStyle/h2" Name="h2"/>
        <ParagraphStyle Self="ParagraphStyle/h3" Name="h3"/>
        <ParagraphStyle Self="ParagraphStyle/p" Name="p"/>
        <ParagraphStyle Self="ParagraphStyle/pFollowsP" Name="p-follows-p"/>
        <ParagraphStyle Self="ParagraphStyle/bulleted-list-item" Name="bulleted-list-item"/>
        <ParagraphStyle Self="ParagraphStyle/numbered-list-item" Name="numbered-list-item"/>
        <ParagraphStyle Self="ParagraphStyle/ct" Name="chapter-title"/>
        <ParagraphStyle Self="ParagraphStyle/chapter-subtitle" Name="chapter-subtitle"/>
        <ParagraphStyle Self="ParagraphStyle/epigraph-prose" Name="epigraph-prose"/>
        <ParagraphStyle Self="ParagraphStyle/epigraph-poetry" Name="epigraph-poetry"/>
        <ParagraphStyle Self="ParagraphStyle/extract-prose" Name="extract-prose"/>
        <ParagraphStyle Self="ParagraphStyle/extract-poetry" Name="extract-poetry"/>
        <ParagraphStyle Self="ParagraphStyle/abbreviation-list-item" Name="abbreviation-list-item"/>
        <ParagraphStyle Self="ParagraphStyle/dialogue-list-item" Name="dialogue-list-item"/>
        <ParagraphStyle Self="ParagraphStyle/series-list-item" Name="series-list-item"/>
        <ParagraphStyle Self="ParagraphStyle/source-note" Name="source-note"/>
        <ParagraphStyle Self="ParagraphStyle/part-number" Name="part-number"/>
        <ParagraphStyle Self="ParagraphStyle/part-title" Name="part-title"/>
        <ParagraphStyle Self="ParagraphStyle/part-subtitle" Name="part-subtitle"/>
        <ParagraphStyle Self="ParagraphStyle/sc-continuous-paragraph" Name="general-text-cont'd"/>
        <ParagraphStyle Self="ParagraphStyle/ornament" Name="ornament"/>

        <!--<ParagraphStyle Self="ParagraphStyle/table" Name="table"/>
        <ParagraphStyle Self="ParagraphStyle/footnote" Name="footnote"/>-->

        <!-- Generate the rest of the ParagraphStyles using the @class value -->
        <xsl:apply-templates select="//xhtml:p[@class]|//xhtml:h1[@class]" mode='paragraph-style'/>
      </RootParagraphStyleGroup>
      <Story Self="xhtml2icml_default_story" AppliedTOCStyle="n" TrackChanges="false" StoryTitle="MyStory" AppliedNamedGrid="n">
        <StoryPreference OpticalMarginAlignment="false" OpticalMarginSize="12" FrameType="TextFrameType" StoryOrientation="Horizontal" StoryDirection="LeftToRightDirection"/>
        <InCopyExportOption IncludeGraphicProxies="true" IncludeAllResources="false"/>
        <xsl:apply-templates/>
      </Story>
      <xsl:apply-templates select=".//xhtml:a" mode="hyperlinks"/>
      <xsl:apply-templates select=".//xhtml:a" mode="hyperlink-url-destinations"/>
    </Document>
  </xsl:template>

  <!-- ==================================================================== -->
  <!-- Headings, paras and block-level elements -->
  <!-- ==================================================================== -->

  <!-- Headings -->
  <xsl:template match="xhtml:h1[not(@class)]|
                       xhtml:h2[not(@class)]|
                       xhtml:h3[not(@class)]">
    <xsl:call-template name="para-style-range">
      <xsl:with-param name="style-name" select="name()"/> <!--h1 comes thru, but no class info -->
<!--      <xsl:with-param name="style-name" select="@class"/> <!-\-class comes thru, but then not P.O. h1-\->-->
    </xsl:call-template>
  </xsl:template>

  <!-- Dynamically-style-named headings -->
  <xsl:template match="xhtml:h1[@class]|
                       xhtml:h2[@class]|
                       xhtml:h3[@class]">
    <xsl:call-template name="para-style-range">
      <xsl:with-param name="style-name" select="@class"/>
    </xsl:call-template>
  </xsl:template>

  <!--Part titles-->
  <xsl:template match="xhtml:section[@class='body-part']//xhtml:h1[@class='ct']">
    <xsl:call-template name="para-style-range">
      <xsl:with-param name="style-name">part-title</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <!-- == Initial-vs-subsequent paragraph treatment ==
       Default paragraph treatment uses the 'p' style. "Normal" (@class-less) paragraphs that
       immediately following normal paragraphs get the 'pFollowsP' style. -->

  <!-- Normal initial paragraphs -->
  <xsl:template match="xhtml:p[not(@class)]">
    <xsl:call-template name="para-style-range">
      <xsl:with-param name="style-name">p</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <!-- Paragraphs that follow paragraphs -->
  <xsl:template match="xhtml:p[not(@class)]
                              [preceding-sibling::*[1][self::xhtml:p[not(@class)]]]
                              ">
    <xsl:call-template name="para-style-range">
      <xsl:with-param name="style-name">pFollowsP</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <!-- General text cont'd -->
  <xsl:template match="xhtml:p[@class='sc-continuous-paragraph']">
    <xsl:call-template name="para-style-range">
      <xsl:with-param name="style-name">general-text-contd</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <!-- Dynamically-style-named paragraphs -->
  <xsl:template match="xhtml:p[@class]">
    <xsl:call-template name="para-style-range">
      <xsl:with-param name="style-name" select="@class"/>
    </xsl:call-template>
  </xsl:template>

<!--  Chapter Subtitle -->
  <xsl:template match="xhtml:p[@class='cst']">
    <xsl:call-template name="para-style-range">
      <xsl:with-param name="style-name">chapter-subtitle</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <!--  Chapter Subtitle -->
  <xsl:template match="xhtml:section[@class='body-part']//xhtml:p[@class='cst']">
    <xsl:call-template name="para-style-range">
      <xsl:with-param name="style-name">part-subtitle</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <!-- Quotes -->
  <xsl:template match="xhtml:blockquote[@class='sep']">
    <xsl:call-template name="para-style-range">
      <xsl:with-param name="style-name">epigraph-prose</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="xhtml:blockquote[@class='sepo']">
    <xsl:call-template name="para-style-range">
      <xsl:with-param name="style-name">epigraph-poetry</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="xhtml:blockquote[@class='ex']">
    <xsl:call-template name="para-style-range">
      <xsl:with-param name="style-name">extract-prose</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="xhtml:blockquote[@class='px']">
    <xsl:call-template name="para-style-range">
      <xsl:with-param name="style-name">extract-poetry</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <!-- Source notes -->
  <xsl:template match="xhtml:cite">
    <xsl:call-template name="para-style-range">
      <xsl:with-param name="style-name">source-note</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <!-- Part numbers -->
  <xsl:template match="xhtml:part-number">
    <xsl:call-template name="para-style-range">
      <xsl:with-param name="style-name">part-number</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <!-- ==================================================================== -->
  <!-- Inlines -->
  <!-- ==================================================================== -->

  <xsl:template match="text()" mode="character-style-range">
    <xsl:call-template name="char-style-range">
      <xsl:with-param name="style-name">normal</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="xhtml:em|xhtml:i" mode="character-style-range">
    <xsl:call-template name="char-style-range">
      <xsl:with-param name="style-name">i</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="xhtml:strong|xhtml:b" mode="character-style-range">
    <xsl:call-template name="char-style-range">
      <xsl:with-param name="style-name">b</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="xhtml:sub" mode="character-style-range">
    <xsl:call-template name="char-style-range">
      <xsl:with-param name="style-name">subscript</xsl:with-param>
      <xsl:with-param name="vertical-position">Subscript</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="xhtml:sup" mode="character-style-range">
    <xsl:call-template name="char-style-range">
      <xsl:with-param name="style-name">superscript</xsl:with-param>
      <xsl:with-param name="vertical-position">Superscript</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="xhtml:small-caps" mode="character-style-range">
    <xsl:call-template name="char-style-range">
      <xsl:with-param name="style-name">small-caps</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <!-- dynamic inline classes -->
  <!--<xsl:template match="xhtml:span[@class]" mode="character-style-range">
    <xsl:call-template name="char-style-range">
      <xsl:with-param name="style-name" select="@class"/>
    </xsl:call-template>
  </xsl:template>-->

  <xsl:template match="xhtml:br" mode="character-style-range">
    <Br/> <!-- TODO: Is this always going to appear in an acceptable location? -->
  </xsl:template>

  <!-- ornaments -->
  <xsl:template match="xhtml:hr">
    <ParagraphStyleRange AppliedParagraphStyle="ParagraphStyle/ornament">
      <CharacterStyleRange AppliedCharacterStyle="CharacterStyle/normal">
        <Content>***</Content>
      </CharacterStyleRange>
      <Br/>
    </ParagraphStyleRange>
  </xsl:template>

  <!-- ==================================================================== -->
  <!-- Lists -->
  <!-- ==================================================================== -->

  <!-- throw out the editoria-compiled notes component -->
  <xsl:template match="xhtml:section[contains(@id, 'comp-notes')]">
  </xsl:template>

  <xsl:template match="xhtml:ol/xhtml:li">
    <xsl:call-template name="para-style-range">
      <xsl:with-param name="style-name">numbered-list-item</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="xhtml:ul/xhtml:li">
    <xsl:call-template name="para-style-range">
      <xsl:with-param name="style-name">bulleted-list-item</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="xhtml:ul[@custom='abbreviations']/xhtml:li[@class='dd']">
    <xsl:call-template name="para-style-range">
      <xsl:with-param name="style-name">abbreviation-list-item</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="xhtml:ul[@custom='qa']/xhtml:li">
    <xsl:call-template name="para-style-range">
      <xsl:with-param name="style-name">dialogue-list-item</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="xhtml:section[@class='front-component']/xhtml:ol/xhtml:li">
    <xsl:call-template name="para-style-range">
      <xsl:with-param name="style-name">series-list-item</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <!-- TODO: What about other children of the li? What about multiple children
       on a single li? -->
  <!--  <xsl:template match="xhtml:ol/xhtml:li[*]|
                       xhtml:ul/xhtml:li[*]">
    <xsl:if test="count(*) &gt; 1">
      <xsl:message terminate="yes">Multi-element list items not handled!
</xsl:message>
    </xsl:if>
    <xsl:if test="*[not(self::xhtml:p)]">
      <xsl:message terminate="yes">Non-paragraph list children not handled!
</xsl:message>
    </xsl:if>
    <xsl:apply-templates/>
  </xsl:template> -->

  <!-- ==================================================================== -->
  <!-- Links -->
  <!-- below, 1 is linked to 2, and 2 is linked to 3 -->
  <!-- ==================================================================== -->

  <!-- 1. handle link elements inline in the HTML -->
  <xsl:template match="xhtml:a[not(@class='inline-note-callout')]" mode="character-style-range">
    <xsl:variable name="hyperlink-key" select="count(preceding::xhtml:a) + 1"/>
    <xsl:variable name="self" select="concat('htss-', $hyperlink-key)"/>
    <xsl:variable name="name" select="."/>
    <CharacterStyleRange>
      <xsl:attribute name="AppliedCharacterStyle">CharacterStyle/link</xsl:attribute>
      <HyperlinkTextSource Self="{$self}" Name="{$name}" Hidden="false">
        <Content><xsl:value-of select="."/></Content>
      </HyperlinkTextSource>
    </CharacterStyleRange>
  </xsl:template>

  <!-- 2. generate link property element for each link -->
  <xsl:template match="xhtml:a[@href]" mode="hyperlinks">
    <xsl:if test='not(@class="inline-note-callout")'>
      <xsl:variable name="hyperlink-key" select="count(preceding::xhtml:a) + 1"/>
      <xsl:variable name="hyperlink-self" select="concat('hs-', $hyperlink-key)"/>
      <xsl:variable name="hyperlink-url-destination-self" select="concat('huds-', $hyperlink-key)"/>
      <xsl:variable name="hyperlink-text-source-self" select="concat('htss-', $hyperlink-key)"/>
      <xsl:variable name="hyperlink-text-source-name" select="."/>
      <xsl:variable name="destination-unique-key" select="$hyperlink-key"/>
      <Hyperlink Self="{$hyperlink-self}"
        Name="{$hyperlink-text-source-name}"
        Source="{$hyperlink-text-source-self}"
        Visible="true"
        DestinationUniqueKey="{$destination-unique-key}">
        <Properties>
          <BorderColor type="enumeration">Black</BorderColor>
          <Destination type="object"><xsl:value-of select="$hyperlink-url-destination-self"/></Destination>
        </Properties>
      </Hyperlink>
    </xsl:if>
  </xsl:template>

  <!-- 3. generate a link destination object for each link -->
  <xsl:template match="xhtml:a[@href]" mode="hyperlink-url-destinations">
    <xsl:if test='not(@class="inline-note-callout")'>
      <xsl:variable name="hyperlink-key" select="count(preceding::xhtml:a) + 1"/>
      <xsl:variable name="hyperlink-text-source-self" select="concat('htss-', $hyperlink-key)"/>
      <xsl:variable name="hyperlink-url-destination-self" select="concat('huds-', $hyperlink-key)"/>
      <xsl:variable name="hyperlink-text-source-name" select="."/>
      <xsl:variable name="destination-unique-key" select="$hyperlink-key"/>
      <HyperlinkURLDestination Self="{$hyperlink-url-destination-self}"
        Name="{$hyperlink-text-source-name}"
        DestinationURL="{@href}"
        DestinationUniqueKey="{$destination-unique-key}"/>
    </xsl:if>
  </xsl:template>

  <!-- TODO: Add support for internal hyperlinks -->
  <!--  <xsl:template match="xhtml:a[not(@href)]" mode="hyperlinks"/>
  <xsl:template match="xhtml:a[not(@href)]" mode="hyperlink-url-destinations"/>-->

  <!-- ==================================================================== -->
  <!-- Tables -->
  <!-- ==================================================================== -->

  <!-- TODO:
         Allow for tables with less than 3 rows
         Make code more idiomatic XSLT (even if functionality isn't increased)
         -->
  <!--<xsl:template match="xhtml:table[@class='docutils']/xhtml:tbody">
    <ParagraphStyleRange AppliedParagraphStyle="ParagraphStyle/table">
      <CharacterStyleRange>
        <Br/>
        <Table HeaderRowCount="0" FooterRowCount="0" AppliedTableStyle="TableStyle/$ID/[Basic Table]" TableDirection="LeftToRightDirection">
          <xsl:attribute name="BodyRowCount">
            <xsl:value-of select="count(child::xhtml:tr)"/>
          </xsl:attribute>
          <xsl:attribute name="ColumnCount">
            <xsl:value-of select="count(child::xhtml:tr[3]/xhtml:td)"/>
          </xsl:attribute>
          <xsl:variable name="columnWidth" select="$table-width div count(xhtml:tr[3]/xhtml:td)"/>
          <xsl:for-each select="xhtml:tr[3]/xhtml:td">
            <Column Name="{position() - 1}" SingleColumnWidth="{$columnWidth}"/>
          </xsl:for-each>
          <xsl:for-each select="xhtml:tr">
            <xsl:variable name="rowNum" select="position() - 1"/>
            <xsl:for-each select="xhtml:td">
              <xsl:variable name="colNum" select="position() - 1"/>
              <xsl:choose>
                <xsl:when test="@colspan">
                  <Cell Name="{$colNum}:{$rowNum}" RowSpan="1" ColumnSpan="{@colspan}" AppliedCellStyle="CellStyle/$ID/[None]" AppliedCellStylePriority="0">
                    <ParagraphStyleRange AppliedParagraphStyle="ParagraphStyle/$ID/NormalParagraphStyle">
                      <CharacterStyleRange AppliedCharacterStyle="CharacterStyle/$ID/[No character style]">
                        <Content>
                          <xsl:value-of select="*|text()"/>
                        </Content>
                      </CharacterStyleRange>
                    </ParagraphStyleRange>
                  </Cell>
                </xsl:when>
                <xsl:otherwise>
                  <Cell Name="{$colNum}:{$rowNum}" RowSpan="1" ColumnSpan="1" AppliedCellStyle="CellStyle/$ID/[None]" AppliedCellStylePriority="0">
                    <ParagraphStyleRange AppliedParagraphStyle="ParagraphStyle/$ID/NormalParagraphStyle">
                      <CharacterStyleRange AppliedCharacterStyle="CharacterStyle/$ID/[No character style]">
                        <Content>
                          <xsl:value-of select="*|text()"/>
                        </Content>
                      </CharacterStyleRange>
                    </ParagraphStyleRange>
                  </Cell>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:for-each>
          </xsl:for-each>
        </Table>
      </CharacterStyleRange>
    </ParagraphStyleRange>
  </xsl:template>

  <xsl:template match="xhtml:tr">
    <xsl:if test="position() &gt; 2">
      <Br/>
    </xsl:if>
  </xsl:template>

  <xsl:template match="xhtml:td">
    <xsl:if test="position() &gt; 2">
      <Br/>
    </xsl:if>
  </xsl:template>-->

  <!-- ==================================================================== -->
  <!-- Images -->
  <!-- ==================================================================== -->
  <xsl:template match="xhtml:img">
<!--    <xsl:variable name="halfwidth" select="@width div 4"/>
    <xsl:variable name="halfheight" select="@height div 4"/>-->
    <xsl:variable name="halfwidth" select="100"/>
    <xsl:variable name="halfheight" select="100"/>
    <ParagraphStyleRange>
      <CharacterStyleRange>
        <Rectangle Self="uec" ItemTransform="1 0 0 1 {$halfwidth} -{$halfheight}">
          <Properties>
            <PathGeometry>
              <GeometryPathType PathOpen="false">
                <PathPointArray>
                  <PathPointType Anchor="-{$halfwidth} -{$halfheight}"
                                 LeftDirection="-{$halfwidth} -{$halfheight}"
                                 RightDirection="-{$halfwidth} -{$halfheight}"/>
                  <PathPointType Anchor="-{$halfwidth} {$halfheight}"
                                 LeftDirection="-{$halfwidth} {$halfheight}"
                                 RightDirection="-{$halfwidth} {$halfheight}"/>
                  <PathPointType Anchor="{$halfwidth} {$halfheight}"
                                 LeftDirection="{$halfwidth} {$halfheight}"
                                 RightDirection="{$halfwidth} {$halfheight}"/>
                  <PathPointType Anchor="{$halfwidth} -{$halfheight}"
                                 LeftDirection="{$halfwidth} -{$halfheight}"
                                 RightDirection="{$halfwidth} -{$halfheight}"/>
                </PathPointArray>
              </GeometryPathType>
            </PathGeometry>
          </Properties>
          <Image Self="ue6" ItemTransform="1 0 0 1 -{$halfwidth} -{$halfheight}">
            <Properties>
              <Profile type="string">$ID/Embedded</Profile>
              <GraphicBounds Left="0" Top="0" Right="{@width div 2}" Bottom="{@height div 2}"/>
            </Properties>
            <xsl:variable name="imguri" select="replace(@src, '/uploads', 'img')"></xsl:variable>
            <Link Self="ueb" LinkResourceURI="file:///{$imguri}"/>
          </Image>
        </Rectangle>
        <Br/>
      </CharacterStyleRange>
    </ParagraphStyleRange>
  </xsl:template>

  <!-- ==================================================================== -->
  <!-- Footnotes -->
  <!-- ==================================================================== -->

  <xsl:template match="xhtml:a[@class='inline-note-callout']" mode="character-style-range">
    <xsl:for-each select=".">
      <xsl:variable name="note-id" select="substring(@href, 2)"></xsl:variable>
      <xsl:call-template name="footnote-content">
        <xsl:with-param name="note-id" select="$note-id"></xsl:with-param>
      </xsl:call-template>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="footnote-content">
    <xsl:param name="note-id"/>
    <xsl:variable name="note-li" select="$principal-root//xhtml:li[contains(@id, $note-id)]"></xsl:variable>
    <Footnote>
      <ParagraphStyleRange AppliedParagraphStyle="ParagraphStyle/footnote">
        <CharacterStyleRange AppliedCharacterStyle="CharacterStyle/normal">
          <!-- InDesign magical footnote character -->
          <Content><xsl:processing-instruction name="ACE">4</xsl:processing-instruction><xsl:value-of select="$note-li" />
          </Content>
        </CharacterStyleRange>
        <Br/>
      </ParagraphStyleRange>
    </Footnote>
  </xsl:template>

  <!-- ==================================================================== -->
  <!-- StyleGroup boilerplate -->
  <!-- ==================================================================== -->

  <!-- Grab only the first instance of each @class -->
  <xsl:template match="xhtml:p[@class]|xhtml:h1[@class]"
                mode="paragraph-style">
    <xsl:variable name="c" select="@class"/>
    <xsl:if test="not(following::xhtml:p[@class = $c]) and
                  not(following::xhtml:h1[@class = $c])">
      <xsl:call-template name="generate-paragraph-style">
        <xsl:with-param name="style-name" select="@class"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template match="xhtml:span[@class]"
                mode="character-style">
    <xsl:variable name="c" select="@class"/>
    <xsl:if test="not(following::xhtml:span[@class = $c])">
      <xsl:call-template name="generate-character-style">
        <xsl:with-param name="style-name" select="@class"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>


  <!-- ==================================================================== -->
  <!-- Named templates -->
  <!-- ==================================================================== -->
  <xsl:template name="para-style-range">
    <!-- The name of the paragraph style in InDesign -->
    <xsl:param name="style-name"/>
    <!-- A string of text that will precede the paragraph's actual content (ex: 'by ')-->
    <xsl:param name="prefix-content" select="''"/>
    <ParagraphStyleRange>
      <xsl:attribute name="AppliedParagraphStyle">
        <xsl:value-of select="concat('ParagraphStyle/', $style-name)"/>
      </xsl:attribute>
      <xsl:if test="$prefix-content != ''">
        <CharacterStyleRange>
          <Content><xsl:value-of select="$prefix-content"/></Content>
        </CharacterStyleRange>
      </xsl:if>
      <xsl:apply-templates select="text()|*" mode="character-style-range"/>
      <Br/>
    </ParagraphStyleRange>
  </xsl:template>


  <xsl:template name="char-style-range">
    <!-- The name of the character style in InDesign -->
    <xsl:param name="style-name"/>
    <xsl:param name="vertical-position" select="0"/>

    <CharacterStyleRange>
      <xsl:attribute name="AppliedCharacterStyle">
        <xsl:value-of select="concat('CharacterStyle/', $style-name)"/>
      </xsl:attribute>
      <xsl:if test="$vertical-position != 0">
        <xsl:attribute name="Position">
          <xsl:value-of select="$vertical-position"/>
        </xsl:attribute>
      </xsl:if>
      <Content><xsl:value-of select="."/></Content>
    </CharacterStyleRange>
  </xsl:template>

  <xsl:template name="generate-paragraph-style">
    <xsl:param name="style-name"/>
    <xsl:if test='$style-name != ("ct" or
                                  "cst" or
                                  "sc-continuous-paragraph")'>
     <ParagraphStyle>
       <xsl:attribute name="Self"><xsl:value-of select="concat('ParagraphStyle/', $style-name)"/></xsl:attribute>
       <xsl:attribute name="Name"><xsl:value-of select="$style-name"/></xsl:attribute>
     </ParagraphStyle>
    </xsl:if>
  </xsl:template>

  <xsl:template name="generate-character-style">
    <xsl:param name="style-name"/>
    <CharacterStyle>
      <xsl:attribute name="Self"><xsl:value-of select="concat('CharacterStyle/', $style-name)"/></xsl:attribute>
      <xsl:attribute name="Name"><xsl:value-of select="$style-name"/></xsl:attribute>
    </CharacterStyle>
  </xsl:template>

</xsl:stylesheet>
