# Editoria HTML-to-ICML

An XSLT sheet designed to convert Editoria's output HTML to an ICML file with corresponding style information

# Styles

Editoria styles map to the following block-level styles and inline character styles in InDesign.

## ICML block-level styles
* h1
* h2
* h3
* p
* p-follows-p (2nd consecutive p and all following, for purposes of indentation)
* general-text-cont'd
* chapter-title
* chapter-subtitle
* part-number
* part-title
* part-subtitle
* epigraph-poetry
* epigraph-prose
* extract-poetry
* extract-prose
* bulleted-list-item
* numbered-list-item
* series-list-item
* abbreviations-list-item
* dialogue-list-item
* source-note
* half-title
* author
* dedication
* publisher
* series-editor
* series-title
* signature
* bibliography-entry
* glossary
* booktitle (tagged from Editoria output HTML, rather than from a specific Editoria style)

## ICML character styles
* italic
* subscript
* superscript
* small-caps

# Other features
* Notes from Editoria are inserted as InDesign footnotes
* Images
  * Images will be exported in the `images/` directory, which are relatively referenced from the ICML. The ICML files shouldn't be moved in relation to these images before the ICML is inserted into an InDesign file.
  * Images can't currently be inserted at their original size. Instead, images are inserted in 100x100px frames. To correctly size images:
    1. right-click the image rectangle
    2. click "Fitting > Fit Frame to Content"